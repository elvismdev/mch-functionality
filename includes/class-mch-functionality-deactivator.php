<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://parenthesis.io/
 * @since      1.0.0
 *
 * @package    Mch_Functionality
 * @subpackage Mch_Functionality/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Mch_Functionality
 * @subpackage Mch_Functionality/includes
 * @author     Parenthesis Tech <support@parenthesis.io>
 */
class Mch_Functionality_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
