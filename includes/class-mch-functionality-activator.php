<?php

/**
 * Fired during plugin activation
 *
 * @link       https://parenthesis.io/
 * @since      1.0.0
 *
 * @package    Mch_Functionality
 * @subpackage Mch_Functionality/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Mch_Functionality
 * @subpackage Mch_Functionality/includes
 * @author     Parenthesis Tech <support@parenthesis.io>
 */
class Mch_Functionality_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
