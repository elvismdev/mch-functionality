<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://parenthesis.io/
 * @since             1.0.0
 * @package           Mch_Functionality
 *
 * @wordpress-plugin
 * Plugin Name:       MCH Functionality
 * Plugin URI:        https://bitbucket.org/parenthesis/mch-functionality
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Parenthesis Tech
 * Author URI:        https://parenthesis.io/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       mch-functionality
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-mch-functionality-activator.php
 */
function activate_mch_functionality() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-mch-functionality-activator.php';
	Mch_Functionality_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-mch-functionality-deactivator.php
 */
function deactivate_mch_functionality() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-mch-functionality-deactivator.php';
	Mch_Functionality_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_mch_functionality' );
register_deactivation_hook( __FILE__, 'deactivate_mch_functionality' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-mch-functionality.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_mch_functionality() {

	$plugin = new Mch_Functionality();
	$plugin->run();

}
run_mch_functionality();
